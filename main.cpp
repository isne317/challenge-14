#include<iostream>

using namespace std;

void quicksort(int[], int, int);
void mergesort(int[], int, int);
void merge(int[], int, int, int);
void swap(int&, int&);

int main()
{
	/*
		Switch sort mode on your own. ;)
	*/
	int array[10] = {5,9,1,7,10,6,4,8,2,3};
	quicksort(array, 0, 10);
	//mergesort(array, 0, 10);
	for(int i = 0; i < 10; i++)
	{
		cout << array[i] << endl;
	}
	return 0;
}

void quicksort(int array[], int start, int size)
{
	/*
		Quick sort.
		Also use swap function.
	*/
	int pivot = array[start];
	int pos = 1, left = 0, right = 0;
	for(int i = start + 1; i < start + size; i++)
	{
		if(array[i] < pivot)
		{
			swap(array[i], array[start + pos]);
			pos++;
			left++;
		}
		else
		{
			right++;	
		}
	}
	swap(array[start], array[(start + pos) - 1]);
	if(left > 1)
	{
		quicksort(array, start, left);	
	}
	if(right > 1)
	{
		quicksort(array, start + pos, right);
	}
}

void swap(int &num1, int &num2)
{
	/*
		For use with quicksort function.
	*/
	int temp = num1;
	num1 = num2;
	num2 = temp;
}

void mergesort(int array[], int low, int high)
{
	/*
		Merge sort.
		Also use merge function.
	*/
    int middle;
    if(low < high)
    {
   		middle = (low + high) / 2;
    	mergesort(array, low, middle);
    	mergesort(array, middle + 1, high);
    	merge(array, low, middle, high);
    }
}

 void merge(int array[], int low, int middle, int high)
{
	/*
		For use with mergesort function.
	*/
    int n1 = middle - low + 1;
    int n2 = high - middle;
    int leftArray[n1 + 1];
    int rightArray[n2 + 1];
    for(int i = 1; i <= n1; i++)
    {
        leftArray[i] = array[low + i - 1];
    }
    for(int j = 1; j <= n2; j++)
	{
        rightArray[j] = array[middle + j];
    }
    leftArray[n1 + 1] = 2147483646;
    rightArray[n2 + 1] = 2147483646;
    int i = 1, j = 1;
    for(int k = low; k <= high; k++)
    {
    	if(leftArray[i] <= rightArray[j])
        {
            array[k] = leftArray[i];
            i++;
        }
        else
        {
            array[k] = rightArray[j];
            j++;
        }
    }
}
